<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Upgrade des tables
 *
 * @param string $nom_meta_base_version
 * @param string $version_cible
 */

function stats_data_upgrade($nom_meta_base_version, $version_cible) {

	$maj = [];
	$maj['create'] = [
		['sql_alter', "spip_referers_articles ADD visites_jour int(10) unsigned not null default '0'"],
		['sql_alter', "TABLE spip_referers_articles ADD visites_veille int(10) unsigned not null default '0'"],
	];
	$maj['1.0.0'] = [

	];


	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Desinstallation
 *
 * @param string $nom_meta_base_version
 */

function date_inscription_vider_tables($nom_meta_base_version) {
	include_spip('inc/meta');
	include_spip('base/abstract_sql');
	sql_alter('TABLE spip_referers_articles DROP visites_jour');
	sql_alter('TABLE spip_referers_articles DROP visites_veille');
	effacer_meta($nom_meta_base_version);
}
