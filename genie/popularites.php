<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_once _DIR_PLUGIN_STATS . 'genie/popularites.php';

/**
 * Cron de calcul des popularités des articles
 *
 * @uses genie_popularite_constantes()
 *
 * @param int $t
 *     Timestamp de la dernière exécution de cette tâche
 * @return int
 *     Positif si la tâche a été terminée, négatif pour réexécuter cette tâche
 **/
function genie_popularites($t) {

	// Une fois par jour purger les referers du jour ; qui deviennent
	// donc ceux de la veille ; au passage on stocke une date_statistiques
	// dans spip_meta - cela permet au code d'etre "reentrant", ie ce cron
	// peut etre appele par deux bases SPIP ne partageant pas le meme
	// _DIR_TMP, sans tout casser...

	// faire le test avant d'appele le genie du core
	$aujourdhui = date('Y-m-d');
	$purger_visites = false;
	if (($d = $GLOBALS['meta']['date_statistiques']) != $aujourdhui) {
		$purger_visites = true;
	}

	// faire les trucs du core
	$res = genie_popularites_dist($t);

	// et ajouter notre purge popularités referers_articles
	if ($purger_visites) {
		spip_log("genie_popularites: on purge les visites de spip_referers_articles (depuis $d)", 'stats_data' . _LOG_DEBUG);

		if (strncmp($GLOBALS['connexions'][0]['type'], 'sqlite', 6) == 0) {
			spip_query('UPDATE spip_referers_articles SET visites_veille=visites_jour, visites_jour=0');
		} else // version 3 fois plus rapide, mais en 2 requetes
			#spip_query("ALTER TABLE spip_referers CHANGE visites_jour visites_veille INT( 10 ) UNSIGNED NOT NULL DEFAULT '0',CHANGE visites_veille visites_jour INT( 10 ) UNSIGNED NOT NULL DEFAULT '0'");
			#spip_query("UPDATE spip_referers SET visites_jour=0");
			// version 4 fois plus rapide que la premiere, en une seule requete
			// ATTENTION : peut poser probleme cf https://core.spip.net/issues/2505
		{
			sql_alter("TABLE spip_referers_articles DROP visites_veille,
			CHANGE visites_jour visites_veille INT(10) UNSIGNED NOT NULL DEFAULT '0',
			ADD visites_jour INT(10) UNSIGNED NOT NULL DEFAULT '0'");
		}
	}

	// et c'est fini pour cette fois-ci
	return $res;
}
